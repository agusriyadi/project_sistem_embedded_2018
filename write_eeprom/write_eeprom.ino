// include the library code:
#include <EEPROM.h>
//#include <LiquidCrystal.h>
#include <SHT1x.h>
int addr = 0;                   // the current address in the EEPROM we're going to write to
//int sensor_pin = 0;             // the analog pin for LM34 temp sensor
//int sensor_reading = 0.0;       // variable to store the value coming from the sensor
//float vref = 4.85;              // variable to store the voltage reference used
                                // using a separate thermometer tweak vref value to fine tune the displayed temperature
// Specify data and clock connections and instantiate SHT1x object
#define dataPin  11
#define clockPin 10
SHT1x sht1x(dataPin, clockPin);

int fahrenheit = 0;             // variable to store the fahrenheit temperature
int centigrade = 0;             // variable to store the centigrade temperature
int acquisition_timer = 1000; // variable to control the time between updates (in ms)
                                // 60000 = 1 min intervals
int loop_counter = 0;
int bytemax = 1024;             // adjust this value for your eeprom memory limit
                                // there are 1024 bytes in the ATmega328 EEPROM
                                // 512 bytes on the ATmega168 and ATmega8
                                // 4 KB (4096 bytes) on the ATmega1280 and ATmega2560

// initialize the library with the numbers of the interface pins
//LiquidCrystal Serial(12, 11, 5, 4, 3, 2);


void setup() {
  // set up the Serial's number of columns and rows: 
  Serial.begin(9600);
  
}

void loop() {
  
  float fahrenheit = sht1x.readTemperatureF();   // calculates the actual fahrenheit temperature
  float centigrade = sht1x.readTemperatureC();;                  //conversion to degrees C

  
   Serial.print(centigrade);
  
   Serial.println("C ");
   Serial.print(fahrenheit);
   
   Serial.println("F");

//Print readings counter on the second row
   
   Serial.print("Readings: ");
   
// if the eeprom is not full 
   if (loop_counter <= bytemax) {
   Serial.println(loop_counter);
   
// divide by 4 because analog inputs range from 0 to 1023 and
// each byte of the EEPROM can only hold a value from 0 to 255.
   int val = centigrade;

// write the value to the appropriate byte of the EEPROM.
// these values will remain there when the board is turned off.
   EEPROM.write(addr, val);//nulis ke eeprom eeprom.write(alamat-eeprom, isi-data)

// advance to the next address.
// there are 1024 bytes in the ATmega328 EEPROM
// 512 bytes on the ATmega168 and ATmega8, 4 KB (4096 bytes) on the ATmega1280 and ATmega2560
  addr = addr + 1;
  delay (acquisition_timer);//delay untuk menulis data berikutnya
  }
  
    else {
      Serial.print("EEPROM Full");
      delay(acquisition_timer);          // 
    }
    loop_counter++;


}


