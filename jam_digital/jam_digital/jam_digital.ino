#include <LiquidCrystal.h>
 // LiquidCrystal display with: // rs on pin 12 // rw on pin 11 // enable on pin 10 // d4-7 on pins 5-2 
LiquidCrystal lcd(12, 11, 10, 5, 4, 3, 2);
int detik=0, menit=0, jam=0;

void setup() { 
  Serial.begin(9600); 
  lcd.begin(2, 16); 
  lcd.clear(); 
  lcd.setCursor(0,0); //(x, y) x = kolom karakter lcd. y = baris karakter lcd 
  lcd.print("JAM DIGITAL");delay(1000); 
  }

  
void loop() { 
    delay(1000);detik++;//generator detik 
    if (detik==60)
    { menit++; detik=0;
      }
    if (menit == 60)
    { jam++; menit=0;
      }
    if (jam == 24)
    { jam = 0;
      }
    Serial.print(jam);Serial.print(":");Serial.print(menit);Serial.print(":");Serial.println(detik);
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print(jam);
    lcd.setCursor(2,0);
    lcd.print(":");
    lcd.setCursor(3,0);
    lcd.print(menit);
    lcd.setCursor(5,0);
    lcd.print(":");
    lcd.setCursor(6,0);
    lcd.print(detik);  
    }
